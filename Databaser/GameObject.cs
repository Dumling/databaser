﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Databaser
{
    abstract class GameObject
    {
        protected Texture2D sprite;
        protected Color color;
        protected Vector2 position;
        protected Vector2 origin;
        protected Vector2 offset;
        private Rectangle rectangle;

        public virtual Rectangle collisionBox
        {
            get
            {
                return new Rectangle(
                    (int)(position.X + offset.X),
                    (int)(position.Y + offset.Y),
                    sprite.Width,
                    sprite.Height
                    );
            }
            set { rectangle = value; }
        }


        public abstract void LoadContent(ContentManager content);

        public abstract void Update(GameTime gameTime);

        public abstract void Draw(SpriteBatch spriteBatch);

        public abstract void OnCollision(GameObject other);

        public void CheckCollision(GameObject other)
        {
            if(collisionBox.Intersects(other.collisionBox))
            {
                OnCollision(other);
            }
        }


    }
}
