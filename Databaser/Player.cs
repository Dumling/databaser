﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    public class Player
    {
        //The name of the player's team.
        private static string teamName;

        //The amount of money the player have,
        private static int money;

        //The total count of team mebers owned by the player.
        private static int ownedCount = 0;

        //Used to set the list of owned team members to a lenght of 12.
        //To prevent the game crashing beacuse of an out of range error, when the buttons on the homescreen asks for information about 
        //the fx. 11th spoton the list.
        public static int[] ar = new int[12];
        /// <summary>
        /// List of IDs for the team members the players own.
        /// <para>Used to tell the buttons on the homescreen what team members to keep track of.</para>
        /// </summary>
        public static List<int> Owned = new List<int>(ar);

        /// <summary>
        /// Gets or sets the player's team name.
        /// <para>When setteing it to a new name the databse is automatically being updated with the new information.</para>
        /// </summary>
        public static string TeamName
        {
            get { return teamName; }
            set { teamName = value;
                //Updates the database.
                sql.SetTeamName(teamName);
            }
        }

        /// <summary>
        /// Gets or sets the player's amoutn of money.
        /// <para>When setteing it to a new value the databse is automatically being updated with the new information.</para>
        /// </summary>
        public static int Money
        {
            get { return money; }
            set { money = value;
                //Updates the databse.
                sql.SetMoney(money);
            }
        }

        /// <summary>
        /// Gets or sets the total amount of team members owned by the player.
        /// <para>When setteing it to a new value the databse is automatically being updated with the new information.</para>
        /// </summary>
        public static int OwnedCount
        {
            get { return ownedCount; }
            set { ownedCount = value;
                //Updates the databse.
                sql.SetPlayerOwnedCount(ownedCount);
            }
        }

        /// <summary>
        /// When the game is being loaded, gets the amount of money the playe have from the database, and sets it in the variabel that stores 
        /// the information.
        /// </summary>
        public static void SetMoney()
        {
            money = sql.GetPlayerMoney();
        }

        /// <summary>
        /// When the game is being loaded, get the player's team naem from the database, and sets it in the variabel that stores the information.
        /// </summary>
        public static void SetTeamName()
        {
            teamName = sql.GetPlayerTeamName();
        }

        /// <summary>
        /// When the game is being loaded, gets the total amount of team members owned by the player from the databse
        /// and stores it in the varibael for it.
        /// </summary>
        public static void SetOwnedCount()
        {
            ownedCount = sql.GetPlayerOwnedCount();
        }
    }
}
