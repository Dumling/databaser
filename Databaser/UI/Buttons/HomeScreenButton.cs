﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Databaser.UI.Buttons
{
    public class HomeScreenButton : Button
    {
        //The index used to telle the button, from where in the list of the cyclist the player owns, the buttons houdl fetch its information.
        private int index;

        //Stores the cycklist information in the button.
        //Below can be seen some placeholder information.
        private string name = "Jens";
        private int speed = 9;
        private int endureance = 5;
        private int power = 1;
        private int price = 100;

        //The id of the cyclist the button is fetching information about.
        private int id;

        /// <summary>
        /// Creatas a button for the homescrene.
        /// <para>The homescreen button is to work with a list for what team mambers the player owns, in the Player class.</para>
        /// </summary>
        /// <param name="position">Where the button is on screen.</param>
        /// <param name="index">What spot in the list of owned team members the button keeps track of.</param>
        public HomeScreenButton(Vector2 position, int index)
        {
            this.position = position;
            this.index = index;

            //Sets the info text, that is used to write additional information, outisde of the button.
            infoText = new Text(new Vector2(1000, 550), "Arial");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //Sets the text that is writton on top of the button, to the name of a members of the player's team.
            text = name;

            //Writes additional information, when the mouse is hovering over the button.
            if (MouseEnterButton())
            {
                infoText.SetText = $"Name: {name}.\nSpeed: {speed}/10.\nEndurance: {endureance}/10.\nPower: {power}/10.\nPrice: {price} kr.\n\nClick to sell";
            }else
            {
                infoText.SetText = "";
            }

            //Tells the button what the id is for the cyclist the button fetches information from.
            //The button gets the ID from a lost if IDs, of cyclist the player owns.
            id = Player.Owned[index];

            //If the id is null, set it to 151 to prevent crashing, this will also make the databas return a bunch of 0s and empty values.
            if (id == null)
            {
                id = 151;
            }

            //If the id isn't 151, get the name of the cyclist the button is keeping track of from the database.
            if (id != 151)
            {
                text = sql.cykelRytterName(id);

            }else
            {
                text = "Empty";
            }
        }

        //If the count of cyclist owned by the player is above 0, and the id of the cyclist to button is keeping track of isn't 0.
        //Sell the cyclist the button is keeping treck of, if the button is pressed.
        public override void ButtonFunction(GameTime gameTime)
        {
            if (Player.OwnedCount > 0 && id != 0)
            {
                //Gives the player money for the sold cyclist.
                Player.Money += sql.cykelRytterPrice(id);
                
                //Sets what team the sold cyclist is on to  (no team).
                sql.SetTeams(id, 0);

                //Updates the list of cyclists owned by the player-
                Player.Owned[index] = 0;

                //Updates the count of cyclists owned by the player.
                Player.OwnedCount--;

                //Updates the buttons' data.
                foreach (Menus m in GameManager.menus)
                {
                    m.UpdateData();
                }
            }
        }

        //Upates the data the button stores.
        public override void UpdateData()
        {
            Debug.WriteLine("Here");

            //Fetches the data from the databse, by sending a request, with the id of the cyclist the buttons is keeping track of.
            name = sql.cykelRytterName(id);
            speed = sql.cykelRytterSpeed(id);
            endureance = sql.cykelRytterEdnurance(id);
            power = sql.cykelRytterPower(id);
            price = sql.cykelRytterPrice(id);
        }
    }
}
