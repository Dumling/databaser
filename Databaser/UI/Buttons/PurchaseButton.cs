﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Databaser
{
    class PurchaseButton : Button
    {
        //Stores information about the different cyclists.
        private string name;
        private int speed;
        private int endurance;
        private int power;
        private int price;
        private string team;
        private int id;

        //Used to make the player wbale to scroll through the list.
        private int previousState;
        private int currentState;

        //Used to reset the button's position.
        private Vector2 startPos;

        /// <summary>
        /// Creates a button to purchase a cyclist.
        /// <para>The pruchase buttons are made to work with information about a specific cyclist in the databse. </para>
        /// <para>Each button gets their own
        /// cyclist to keep track of.</para>
        /// </summary>
        /// <param name="position">Where hte button is on screen.</param>
        /// <param name="id">The id of the cyclist the button keeps track od.</param>
        /// <param name="name">The name of the cyclist.</param>
        /// <param name="speed">The speed of the cyclist.</param>
        /// <param name="endurance">The endurance of the cyclist.</param>
        /// <param name="power">The power of the cyclist.</param>
        /// <param name="price">The price of the cyclist.</param>
        /// <param name="team">What team the cylist is on.</param>
        public PurchaseButton(Vector2 position, int id, string name, int speed, int endurance, int power, int price, string team)
        {
            this.position = position;
            this.id = id;
            this.name = name;
            this.speed = speed;
            this.endurance = endurance;
            this.power = power;
            this.price = price;
            this.team = team;
            text = $"{name} - {team}";
            startPos = position;

            //Sets the info text, that writes additioanl information outside of the button.
            infoText = new Text(new Vector2(1250, 450), "Arial");
        }

        //Allows the player to buy the cyclist, when they clik on the button.
        public override void ButtonFunction(GameTime gameTime)
        {
            //If the cyclist isn't owned by any team.
            if(team == "No team")
            {
                //If the player have more money than the cyclist costs, and own less than 12 team mambers.
                //Let the player buy the cyclist.
                if (Player.Money >= sql.cykelRytterPrice(id) && Player.OwnedCount < 12)
                {
                    //Updates the cyclist's team to the player's team (1).
                    sql.SetTeams(id, 1);

                    //Update the data for the button.
                    UpdateData();

                    //Charge the player for the cyclist.
                    Player.Money -= sql.cykelRytterPrice(id);

                    //Update the list of cyclists owned by the player, to add the one the player just bought.
                    for(int i = 0; i < 12; i++)
                    {
                        if(Player.Owned[i] == 0)
                        {
                            Player.Owned[i] = id;
                            break;
                        }

                    }

                    //Update the cunt of team members owned by the player.
                    Player.OwnedCount++;

                    //Update the data on all buttons.
                    foreach(Menus m in GameManager.menus)
                    {
                        m.UpdateData();
                    }

                }else
                {
                    Debug.WriteLine("Ikke nok penge");
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //Alows the player to scroll.
            Scrolling();

            //String that stores information about the team.
            string teamState = "";

            //If the cyclist isn't owned by a team, write the price.
            //If the cyclist is owned by a team, write the team name.
            if(team == "No team")
            {
                teamState = $"{price} kr.";
            }else
            {
                teamState = $"({team})";
            }

            //Wrties the text on the button.
            //Wrties the cyclists name and the price or team the cyclist have.
            text = $"{name}\n{teamState}";
            
            //If the mouse is hovering over the button.
            if(MouseEnterButton())
            {
                //If the cyclist isn't owned by a team, tell the player they can buy it.
                //If the cyclist is owned by a team, write the team name.
                if (team == "No team")
                {
                    infoText.SetText = $"{name}\n\n" +
                        $"Speed: {speed}\n" +
                        $"Endurance: {endurance}\n" +
                        $"Power: {power}\n" +
                        $"Price: {price}\n\n" +
                        $"Click to purhcase";
                }else
                {
                    infoText.SetText = $"{name}\n\n" +
                    $"Speed: {speed}\n" +
                    $"Endurance: {endurance}\n" +
                    $"Power: {power}\n" +
                    $"Owned by {team}";
                }
            }
            else //Else, don't write anything.
            {
                infoText.SetText = "";
            }
        }

        /// <summary>
        /// Makes the player able to scroll through the list of buttons.
        /// </summary>
        private void Scrolling()
        {
            currentState = Mouse.GetState().ScrollWheelValue;

            if (currentState < previousState)
            {
                position += new Vector2(0, -100);
            }
            else if (currentState > previousState)
            {
                position += new Vector2(0, 100);
            }

            previousState = currentState;
        }

        //Resets the button.
        public override void Reset()
        {
            position = startPos;
        }

        //Updates the data that the button stores.
        public override void UpdateData()
        {
            team = sql.cykelRytterTeam(id);
        }
    }
}
