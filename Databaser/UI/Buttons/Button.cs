﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Data.SQLite;
using System.Diagnostics;

namespace Databaser
{
    public abstract class Button
    {
        /// <summary>
        /// The button's sprite.
        /// </summary>
        protected Texture2D sprite;
        
        /// <summary>
        /// The text that is written on top of the button.
        /// </summary>
        protected string text = "";

        /// <summary>
        /// The font used to write the text on top of the button.
        /// </summary>
        protected SpriteFont font;

        /// <summary>
        /// String used to tell the buttons what font to use to write text on the top of the button.
        /// </summary>
        protected string fontName;

        /// <summary>
        /// The color of the button.
        /// </summary>
        protected Color color = Color.White;

        /// <summary>
        /// The position the buttons have on the game screen.
        /// </summary>
        protected Vector2 position;

        //Used to make sure the button only reacts to a mouse click once.
        protected MouseState newMouseState;
        protected MouseState oldMouseState;

        /// <summary>
        /// Infotext written outside of the button, to show additional information.
        /// <para>Each sub class must overwrite these standard setting, by defining it with new values, if they need it.</para>
        /// </summary>
        protected Text infoText = new Text(Vector2.Zero, "Arial");

        public virtual void LoadContent(ContentManager content)
        {
            //Loads the infotext.
            infoText.LoadContent(content);

            //Loads all content needed for the buttons to work.
            sprite = content.Load<Texture2D>("UI/Sprites/button2");
            
            //If no font name is given, and is null, load default font, ButtonFont, to prevent game crashes.
            if(fontName == null)
            {
                font = content.Load<SpriteFont>("UI/Fonts/ButtonFont");
            }else
            {
                font = content.Load<SpriteFont>($"UI/Fonts/{fontName}");
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            //Updates the info text written outside of the button.
            infoText.Update(gameTime);

            //Changes the buttons color, to show it's highlighted.
            if(MouseEnterButton())
            {
                color = Color.Blue;
            }else
            {
                color = Color.White;
            }

            //Makes the button clickable.
            //Also ensures the button only reacts once to a mouse click.
            newMouseState = Mouse.GetState();

            if(MouseEnterButton() && oldMouseState.LeftButton == ButtonState.Released && newMouseState.LeftButton == ButtonState.Pressed)
            {
                ButtonFunction(gameTime);
            }

            oldMouseState = newMouseState;
        }

        /// <summary>
        /// What happens when the button is clicked.
        /// <para>Code added here in the sub classes.</para>
        /// </summary>
        /// <param name="gameTime"></param>
        public abstract void ButtonFunction(GameTime gameTime);

        /// <summary>
        /// Checks if the mouse is hovering above the button.
        /// </summary>
        /// <returns></returns>
        protected bool MouseEnterButton()
        {
            if (Mouse.GetState().X < (position.X - sprite.Width / 2) + sprite.Width && Mouse.GetState().X > (position.X - sprite.Width / 2) && Mouse.GetState().Y < (position.Y - sprite.Height / 2) + sprite.Height && Mouse.GetState().Y > (position.Y - sprite.Height / 2))
            {
                return true;
            }

            return false;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            //Draws the button.
            if (sprite != null)
            {
                spriteBatch.Draw(sprite, new Vector2(position.X - sprite.Width / 2, position.Y - sprite.Height / 2), color);
            }

            //The draws the text on the button.
            if (font != null)
            {
                spriteBatch.DrawString(font, text, new Vector2(position.X - (font.MeasureString(text).X / 2), position.Y - (font.MeasureString(text).Y / 2)), Color.Black);
            }

            //Draws the text outside of the button, if needed.
            infoText.Draw(spriteBatch);
        }

        /// <summary>
        /// Makes the buttons able ot be reset if needed.
        /// <para>The sub classes must add what is being reset.</para>
        /// </summary>
        public virtual void Reset()
        {

        }

        /// <summary>
        /// Makes the button update the data that they store.
        /// <para>The sub classes must add what data is being updated.</para>
        /// </summary>
        public virtual void UpdateData()
        {

        }
    }
}
