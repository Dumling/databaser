﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Data.SQLite;
using System.Diagnostics;

namespace Databaser.UI.Buttons
{
    class MiscButton : Button
    {
        //What the button does.
        private int input;

        /// <summary>
        /// Creates a misc button.
        /// <para>1: New game.</para>
        /// <para>2: Load game.</para>
        /// <para>3: Race screen.</para>
        /// <para>4: Purchase screen.</para>
        /// <para>5: Return to home screen.</para>
        /// </summary>
        /// <param name="position"></param>
        /// <param name="input">What the button do.</param>
        /// <param name="text"></param>
        public MiscButton(Vector2 position, int input, string text)
        {
            this.position = position;
            this.input = input;
            this.text = text;
        }

        //What happens when the player clicks on the button.
        public override void ButtonFunction(GameTime gameTime)
        {
            //Switch case for what the button does, determined by the input.
            switch(input)
            {
                //MenuIndex: StartScreen = 0
                //MenuIndex: HomeScreen = 1
                //MenuIndex: PurchaseScreen = 2
                //MenuIndex: RaceScreen = 3

                case 1: //Starts new game.
                    GameManager.NewGame();
                    GameManager.MenuIndex = 4;
                    break;

                case 2: //Loads game
                    GameManager.LoadGame();
                    GameManager.MenuIndex = 1;
                    break;

                case 3: //Opens Race screen.
                    if (Player.OwnedCount > 1)
                    {
                        GameManager.MenuIndex = 3;
                    }
                    break;

                case 4: //Open purchase screen/menu.
                    GameManager.MenuIndex = 2;
                    break;

                case 5: //Goes back to the home screen.
                    GameManager.MenuIndex = 1;
                    break;

                default: //If none of the above, error.
                    Debug.WriteLine("ERROR: Invalid input.");
                    break;
            }
        }

    }
}
