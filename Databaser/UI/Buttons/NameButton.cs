﻿using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser.UI.Buttons
{
    class NameButton : Button
    {
        //Stores the name the button offers to the player.
        private string name;

        /// <summary>
        /// Creates a button that lets the palyer chose their team name.
        /// </summary>
        /// <param name="position">The posiiton the button has on screen.</param>
        /// <param name="name">The team name the button offers to the player.</param>
        public NameButton(Vector2 position, string name)
        {
            this.position = position;
            this.name = name;
            text = name;
        }

        //If the button is cliked, set the player's team name, and go to home screen (menu index 1).
        public override void ButtonFunction(GameTime gameTime)
        {
            Player.TeamName = name;
            GameManager.MenuIndex = 1;
        }
    }
}
