﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;


namespace Databaser
{
    public class Text
    {
        /// <summary>
        /// The text that is being written.
        /// </summary>
        protected string text = "";

        /// <summary>
        /// The font used to write the text.
        /// </summary>
        protected SpriteFont font;

        /// <summary>
        /// The name of the font, that is being loaded, to write the text.
        /// Set to Arial by default.
        /// </summary>
        protected string fontName = "Arial";

        /// <summary>
        /// The color of the text.
        /// Set to black by default.
        /// </summary>
        protected Color textColor = Color.Black;

        /// <summary>
        /// Where the text is on screen.
        /// </summary>
        protected Vector2 position = Vector2.Zero;

        /// <summary>
        /// Updates the text that is being written.
        /// </summary>
        public string SetText
        {
            set { text = value; }
        }

        /// <summary>
        /// Updates the color of the text that is being written.
        /// </summary>
        public Color SetColor
        {
            set { textColor = value; }
        }

        /// <summary>
        /// Creates a UI text element.
        /// </summary>
        /// <param name="position">Where the text is on the screen.</param>
        /// <param name="fontName">The name of the font used to write the text.</param>
        public Text(Vector2 position, string fontName)
        {
            this.position = position;
            this.fontName = fontName;
        }

        /// <summary>
        /// Creates an UI text element.
        /// </summary>
        /// <param name="position">Where the text is being written on screen.</param>
        /// <param name="text">What text is being written.</param>
        /// <param name="fontName">The name of the font used to write the text.</param>
        public Text(Vector2 position, string text, string fontName)
        {
            this.position = position;
            this.fontName = fontName;
            this.text = text;
        }

        public virtual void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>($"UI/Fonts/{fontName}");
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            //If the text is null set it to "", before it's being written..
            //Done to prevent game crashes
            if(text == null)
            {
                text = "";
            }

            spriteBatch.DrawString(font, text, position, textColor);
        }
    }
}
