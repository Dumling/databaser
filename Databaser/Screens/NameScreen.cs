﻿using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;


namespace Databaser.Screens
{
    class NameScreen : Menus
    {
        //Stores the names that is given to the buttons.
        string input;

        /// <summary>
        /// The screen that lets the player choose a name for their team.
        /// </summary>
        public NameScreen()
        {
            //Creates 12 buttons, each with a name the player can choose for their team.
            //The names are picked randomly from a table of names in the database.
            for (int i = 0; i < 2; i++)
            {
                for (int y = 0; y < 6; y++)
                {
                    input = sql.GetATeamName(new Random().Next(1, 200));
                    buttons.Add(new NameButton(new Vector2(200 + (386 * i), 200 + (150 * y)), input));
                }
            }

            //Creates a text that tells the player to pick a team name.
            texts.Add(new Text(new Vector2(GameManager.ScreenSize.X / 2, 10), "Pick a name", "TameName"));
        }
    }
}
