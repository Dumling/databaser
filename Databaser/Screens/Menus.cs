﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;

namespace Databaser
{
    public abstract class Menus
    {
        /// <summary>
        /// The UI buttons on the screen/menu.
        /// <para>Automatically being loaded, updated and drawn byt the Mnues super class.</para>
        /// </summary>
        protected List<Button> buttons = new List<Button>();
        
        /// <summary>
        /// The UI text element on the screen/menu.
        /// <para>Automatically being loaded, updated and drawn by the Menus super class.</para>
        /// </summary>
        protected List<Text> texts = new List<Text>();

        public virtual void LoadContent(ContentManager content)
        {
            //Loads the button.
            foreach (Button b in buttons)
            {
                b.LoadContent(content);
            }

            //Loads the texts.
            foreach(Text t in texts)
            {
                t.LoadContent(content);
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            //Updates the buttons.
            foreach (Button b in buttons)
            {
                b.Update(gameTime);
            }

            //Updates the texts.
            foreach (Text t in texts)
            {
                t.Update(gameTime);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            //Draws the buttons.
            foreach (Button b in buttons)
            {
                b.Draw(spriteBatch);
            }

            //Draws the texts.
            foreach (Text t in texts)
            {
                t.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// Calls the UpdateData() moethos on all buttons.
        /// </summary>
        public void UpdateData()
        {
            foreach(Button b in buttons)
            {
                b.UpdateData();
            }
        }
    }
}
