﻿using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    class HomeScreen : Menus
    {
        /// <summary>
        /// The screen where the player can manage/sell team members.
        /// Starts reaces. 
        /// Or go to the screen/menu for buying more team members.
        /// </summary>
        public HomeScreen()
        {
            //Creates UI text elements that writes the player's team name, and the amount of money they have.
            texts.Add(new Text(new Vector2(20, 25), "TameName")); //The Player's team name.
            texts.Add(new Text(new Vector2(GameManager.ScreenSize.X - 500, 25), "TameName")); //The players money.

            //Creates 12 buttons, to display the player's team members, and to allow the plater to manage their team.
            for(int i = 0; i < 2; i++)
            {
                for (int y = 0; y < 6; y++)
                {
                    buttons.Add(new HomeScreenButton(new Vector2(200 + (386 * i), 200 + (150 * y)), y+(6*i)));
                }
            }

            //Creates a button for going to the screen/menu to buy more team members.
            //And to start a race.
            buttons.Add(new MiscButton(new Vector2(1400, 200), 4, "Buy team members"));
            buttons.Add(new MiscButton(new Vector2(1000, 200), 3, "Race"));
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //Updates the text for the player's team name and the money they have.
            texts[0].SetText = Player.TeamName;
            texts[1].SetText = $"{Player.Money} kr.";
        }
    }
}
