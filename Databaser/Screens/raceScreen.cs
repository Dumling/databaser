﻿using Databaser.Screens;
using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    public class RaceScreen : Menus
    {
        //Number of cyclists that have finished their race.
        private static int finishedCount = 0;

        /// <summary>
        /// The Number of cyclists that have finished their race.
        /// </summary>
        public static int FinishedCount
        {
            get { return finishedCount; }
            set { finishedCount = value; }
        }

        /// <summary>
        /// The screen where the cyclists is racing.
        /// </summary>
        public RaceScreen()
        {
            //Creates the cyclists and the race track.
            buttons.Add(new RaceTrack(new Vector2(GameManager.ScreenSize.X / 2, (GameManager.ScreenSize.Y / 2) - 150)));
            buttons.Add(new Cyclist(new Vector2(GameManager.ScreenSize.X / 2 - 240, (GameManager.ScreenSize.Y / 2) - 150), "sprites/racers/sprt_racer_1" ));
            buttons.Add(new Cyclist(new Vector2(GameManager.ScreenSize.X / 2 - 240, (GameManager.ScreenSize.Y / 2) - 150), "sprites/racers/sprt_racer_2"));
            buttons.Add(new Cyclist(new Vector2(GameManager.ScreenSize.X / 2 - 240, (GameManager.ScreenSize.Y / 2) - 150), "sprites/racers/sprt_racer_3"));
            
        }


        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //If all 3 cyclists have finised.
            if (finishedCount >= 3)
            {
                //Show resykt screen.
                GameManager.MenuIndex = 5;

                //Choose a winner.
                ResultScreen.ChooseWinner();

                //Reset the counter.
                finishedCount = 0;

                //Reset all cyclists.
                foreach (Button b in buttons)
                {
                    b.Reset();
                }
            }
        }
    }
}
