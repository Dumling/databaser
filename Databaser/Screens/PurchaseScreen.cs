﻿using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    class PurchaseScreen : Menus
    {
        /// <summary>
        /// The screen/menu where the player can buy new team members.
        /// </summary>
        public PurchaseScreen()
        {
            //Creates 150 pruchase buttons, one for each cyclist in the databse, that the play can lick on to buy that cylist.
            for (int i = 0; i < 3; i++)
            {
                for (int y = 1; y < 51; y++)
                {
                    //The buttons gets their information from the database.
                    buttons.Add(new PurchaseButton(new Vector2(200 + (386 * i), 200 + (150 * y)), y + (50 * i), sql.cykelRytterName(y + (50*i)), sql.cykelRytterSpeed(y + (50 * i)), sql.cykelRytterEdnurance(y + (50 * i)), sql.cykelRytterPower(y + (50 * i)), sql.cykelRytterPrice(y + (50 * i)), sql.cykelRytterTeam(y + (50 * i))));
                }
            }

            //Adds a button to go back to the home screen.
            buttons.Add(new MiscButton(new Vector2(1450, 950), 5, "Back"));

            //Adds a text the write the amount of money the player have, and the total count for how mnay team members the player owns.
            texts.Add(new Text(new Vector2(GameManager.ScreenSize.X - 500, 25), "TameName")); //The players money.
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //Updates the text for money and total amount of owned team members.
            texts[0].SetText = $"{Player.Money} kr.\n({Player.OwnedCount}/12)";
        }

    }
}
