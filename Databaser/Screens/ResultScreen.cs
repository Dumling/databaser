﻿using Databaser.UI.Buttons;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace Databaser.Screens
{
    class ResultScreen : Menus
    {
        //What place the player got in the race.
        private static int playerResult;

        //The name of the team that got first place.
        private static string first;

        //The name of the team that got second place.
        private static string second;

        //The name of the team that got third place.
        private static string third;

        //List of teams that has been picked for 1st, 2nd or 3rd place.
        //Used to prevent the game for picking the same team on two places.
        private static List<string> winnerList = new List<string>();

        //The text that writes the results.
        private static Text resultText = new Text(new Vector2(GameManager.ScreenSize.X / 2 - 200, GameManager.ScreenSize.Y / 2 - 250), "Arial");

        /// <summary>
        /// The screen that shows the result of the race.
        /// </summary>
        public ResultScreen()
        {
            buttons.Add(new MiscButton(new Vector2(GameManager.ScreenSize.X / 2, GameManager.ScreenSize.Y - 150), 5, "Continue"));
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            //Loads the result text.
            resultText.LoadContent(content);
        }

        /// <summary>
        /// Chooses the winners.
        /// </summary>
        public static void ChooseWinner()
        {
            //Clears the list of winner names.
            winnerList.Clear();

            //Gives the player the result for what place they got in the race, random number between 1 and 8.
            playerResult = new Random().Next(1, 8);

            //Calculates how much money the player earned, based on their result.
            //The player can earn up to 8000, that is devided by their result to lower the amount, if they didn't get 1st place.
            int earnings = Convert.ToInt32(8000 / playerResult);
            Player.Money = Player.Money + earnings;

            //Choses other teams for the score board, and writes it.
            //If the player didn't make it to top 3.
            if(playerResult > 3)
            {
                //Picks a random team for 1st place.
                first = PickATeam();
                //Adds that team to the list of winner teams, so the game doesn't pick that team for 2nd or 3rd place.
                winnerList.Add(first);

                //Reapeats the proces until a team has been assigned to 2nd and 3rd place´.
                second = PickATeam();
                winnerList.Add(second);
                third = PickATeam();
                winnerList.Add(third);

                //Writes the score board, and what place the player got and how much money the player earned.
                resultText.SetText = $"1. {first}\n2. {second}\n3. {third}\n\nYou got {playerResult} place.\nYou earned {earnings} kr.";

            }else
            {
                //If the player made it into top 3.
                //If the player got 1st place.
                if(playerResult == 1)
                {
                    //Gives the player first place on the score board.
                    first = sql.GetTameName(1);

                    //Assigns random teams on 2nd and 3rd place, using same methos as seena above.
                    second = PickATeam();
                    winnerList.Add(second);
                    third = PickATeam();
                    winnerList.Add(third);
                }else if(playerResult == 2) //If the player got 2nd place.
                {
                    //Same method as seen with if the player got 1st place
                    first = PickATeam();
                    winnerList.Add(first);
                    second = sql.GetTameName(1);
                    third = PickATeam();
                    winnerList.Add(third);
                }else if(playerResult == 3) //If the player got 3rd place.
                {
                    //Same method as seen with if the player got 1st place
                    first = PickATeam();
                    winnerList.Add(first);
                    second = PickATeam();
                    winnerList.Add(second);
                    third = sql.GetTameName(1);
                }

                //Writes the results, and how much money the player got.
                resultText.SetText = $"1. {first}\n2. {second}\n3. {third}\n\nYou got {playerResult} place.\nYou earned {earnings} kr.";
            }
        }

        /// <summary>
        /// Picks a random team member.
        /// </summary>
        /// <returns></returns>
        private static string PickATeam()
        {
            //The string that is used to store what team is being assigned.
            string output = "";

            //Bool to stop the process of picking a team.
            bool i = true;

            //While loop that runs the process of picking a team.
            while (i)
            {
                //Picks a random team from the table with team names in the databse, that isn't the player's team.
                output = sql.GetTameName(new Random().Next(2, 8));

                //If that team isn't already on the list of teams that has already been picked.
                if (!winnerList.Contains(output))
                {
                    //Stop the loop,
                    i = false;
                }

                //Else, the loop will run again and again until it picks a team that hasn't been picked already.
            }

            //Returns the team name.
            return output;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            //Writes the result text.
            resultText.Draw(spriteBatch);
        }
    }
}
