﻿using Databaser.UI.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser.Screens
{
    class StartScreen : Menus
    {
        //The game's logo.
        private Texture2D logo;

        /// <summary>
        /// Creates the screen where the game asks if the player wants to start a new game, or load their previous game.
        /// </summary>
        public StartScreen()
        {
            //Creates the elements used for the screen.
            buttons.Add(new MiscButton(new Vector2(GameManager.ScreenSize.X / 2, GameManager.ScreenSize.Y / 2 - 100), 1, "New Game"));
            buttons.Add(new MiscButton(new Vector2(GameManager.ScreenSize.X / 2, GameManager.ScreenSize.Y / 2 + 100), 2, $"Load Game\n({sql.GetPlayerTeamName()})"));
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            //Loads the logo.
            logo = content.Load<Texture2D>("sprites/logo1");
        }

        //Resets the databae when Delete is pressed, used for debugging.
#if DEBUG
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if(Keyboard.GetState().IsKeyDown(Keys.Delete))
            {
                sql.Reset();
            }
        }
#endif

        public override void Draw(SpriteBatch spriteBatch)
        {
            //Draws the logo.
            spriteBatch.Draw(logo, new Vector2(GameManager.ScreenSize.X / 2 - logo.Width / 2, 70), Color.White);

            base.Draw(spriteBatch);            
        }
    }
}
