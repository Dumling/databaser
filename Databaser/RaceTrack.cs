﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    class RaceTrack : Button
    {
        /// <summary>
        /// Creates the race track for the cyclists.
        /// <para>Must be added as a button.</para>
        /// </summary>
        /// <param name="position"></param>
        public RaceTrack(Vector2 position)
        {
            this.position = position;
        }

        //Overrites the Button super class' load method, to override the default sprite used for buttons.
        public override void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("sprites/racetracks/sprt_racetrack_2");
        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (sprite != null)
            {
                spriteBatch.Draw(sprite, new Vector2(position.X - sprite.Width / 2, position.Y - sprite.Height / 2), color);
            }
        }

        //Not used.
        public override void ButtonFunction(GameTime gameTime)
        {

        }
    }
}
