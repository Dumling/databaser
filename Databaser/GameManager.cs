﻿using Databaser.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;

namespace Databaser
{
    public class GameManager : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        
        /// <summary>
        /// Deltatime.
        /// </summary>
        public float DeltaTime { get; set; }

        //The game's background.
        private Texture2D background;

        /// <summary>
        /// The list of menus/screen the game have.
        /// </summary>
        public static List<Menus> menus = new List<Menus>();

        //Tells the game what menu/screen to show and update.
        private static int menuIndex = 0;

        /// <summary>
        /// Sets what menu/screen is being shown and updated.
        /// </summary>
        public static int MenuIndex
        {
            get { return menuIndex; }
            set { menuIndex = value;  }
        }

        /// <summary>
        /// The screensize of the game.
        /// </summary>
        public static Vector2 ScreenSize;

        public GameManager()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //Sets the window size of the game, and screensize.
            _graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            _graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            ScreenSize = new Vector2(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);
            _graphics.ApplyChanges();

            //Sets the title in the window.
            Window.Title = "Bicycle Racers";

            //Creats the different menus/screen.
            menus.Add(new StartScreen());       //0
            menus.Add(new HomeScreen());        //1
            menus.Add(new PurchaseScreen());    //2
            menus.Add(new RaceScreen());        //3
            menus.Add(new NameScreen());        //4
            menus.Add(new ResultScreen());      //5

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            //Loads the background.
            background = Content.Load<Texture2D>("sprites/backgrounds/sprt_background_1");

            //Calls the load methos on each menu/screen-
            foreach (Menus m in menus)
            {
                m.LoadContent(Content);
            }

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            //Closes the game when ESC is pressed.
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //Calulates deltatime.
            DeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;


            //Updates the menu/screen currently being drawn.
            menus[menuIndex].Update(gameTime);          

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            //Draws the backgorund.
            _spriteBatch.Draw(background,Vector2.Zero,Color.White);

            //Draws the menu/screen currently being shown.
            menus[menuIndex].Draw(_spriteBatch);

            _spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }


        /// <summary>
        /// Resets the teams to 0 (no team).
        /// </summary>
        private static void ResetTeams()
        {          
            for(int i =1; i < 151; i++)
            {
                Debug.WriteLine(i);
                sql.SetTeams(i, 0);               
            }
        }

        /// <summary>
        /// Gives the other teams a random amount of cyclists.
        /// </summary>
        private static void SetTeams()
        {
            //Each of the other teams, that isn't the palyer's are giving a random amount of clyclist, between 2 and 12
            //Then the databse is updated with this information.
            for(int i = 2; i < 8; i++)
            {
                int count = new Random().Next(2, 12);
                for(int y = 0; y < count; y++)
                {
                    sql.SetTeams(new Random().Next(1, 150), i);
                }
            }
        }

        /// <summary>
        /// Resets the databse for a new game.
        /// </summary>
        public static void NewGame()
        {
            //Resets the teams.
            ResetTeams();

            //Sets the teams.
            SetTeams();

            //Resets the player's money.
            Player.Money = 5000;

            //Resets the player's owned count.
            Player.OwnedCount = 0;

            //Updates the buttons.
            foreach(Menus m in menus)
            {
                m.UpdateData();
            }
        }

        /// <summary>
        /// Loads the games.
        /// </summary>
        public static void LoadGame()
        {
            //Loads the player's money, team name and count of owned team members.
            Player.SetMoney();
            Player.SetTeamName();
            Player.SetOwnedCount();

            //Loads the team members owned by the player.
            for(int i = 0; i < 151; i++)
            {
                string input = sql.cykelRytterTeam(i);
                if(input == Player.TeamName)
                {
                    for (int y = 0; y < 12; y++)
                    {
                        if (Player.Owned[y] == 0)
                        {
                            Player.Owned[y] = i;
                            break;
                        }

                    }
                }
            }

            //Updaets the buttons' data.
            foreach(Menus m in menus)
            {
                m.UpdateData();
            }
        }
    }
}
