﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Databaser
{
    class Cyclist : Button
    {
        //Bool for if the cyclist have finished its race.
        private bool finishedRace = false;

        //Used to overrite the Button superplace sprite name.
        private string spriteName;

        //The speed of the cyclist.
        private float speed;

        //The velocity of the cyclist.
        private Vector2 velocity;

        //Random numer generator, for the cyclists speed.
        private static Random rnd = new Random();

        //The start position of the cyclist.
        private Vector2 startPos;

        /// <summary>
        /// Creates a cyclist.
        /// <para>Must be added under buttons.</para>
        /// </summary>
        /// <param name="position"></param>
        /// <param name="spriteName"></param>
        public Cyclist (Vector2 position, string spriteName)
        {
            this.position = position;
            this.speed = (float)rnd.Next(5, 15) / 10;
            this.velocity = new Vector2 (1,0);
            this.spriteName = spriteName;

            startPos = position;

        }

        public override void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>(spriteName);
        }

        public override void Update(GameTime gameTime)
        {
            Move();  
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (sprite != null)
            {
                spriteBatch.Draw(sprite, new Vector2(position.X - sprite.Width / 2, position.Y - sprite.Height / 2), color);
            }
        }

        /// <summary>
        /// Gets the cyclist to move.
        /// </summary>
        public void Move()
        {
            if (position.X < 1200)
            {
                position += velocity * speed;
            }
            else if (!finishedRace) //If the cyclist hvae finished the race.
            {
                //Mark it as such.
                finishedRace = true;

                //Add one to the counter for how many cyclists have finished their race, on the RaceScreen.
                RaceScreen.FinishedCount++;
            }
            
        }

        //Not used.
        public override void ButtonFunction(GameTime gameTime)
        {
            
        }

        //Resets the cyclist's position, when the race is over, and mark it as it have not completed the race.
        public override void Reset()
        {
            position = startPos;
            finishedRace = false;
        }
    }
}
