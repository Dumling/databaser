﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;

namespace Databaser
{
    class sql
    {
        //Informs the program where the database is located, that is being connected to.
        private static SQLiteConnection con = new SQLiteConnection("Data Source=" + @"..\..\..\Databaser\CykelDatabase.db");

        //Used to send commands to the databse.
        private static SQLiteCommand com;

        //Used to read information from the databse.
        private static SQLiteDataReader reader;     

        //Cykerytere 

        /// <summary>
        /// Gets the name of a given cykelrytter.
        /// </summary>
        /// <param name="id">The id the ckelrytter have in the table in the database.</param>
        /// <returns></returns>
        public static string cykelRytterName(int id)
        {
            //Opens the connection.
            con.Open();

            //The string where we store the information from the database.
            string input = "";

            //The command sent to the databse.
            //The commands asks for information from the cykelrytere table, from the row where the id is equeal to the requested id.
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";

            //Sends the command to the database, that the connections is open to.
            com = new SQLiteCommand(stm, con);

            //Starts the SQLite reader.
            reader = com.ExecuteReader();

            //While the reader is reading from the database.
            while (reader.Read())
            {
                //Fets information from second colun (the count starts at 0), from the row the command requested information from
                //and store it in the input string.
                input = reader.GetString(1);
            }

            //Close the connection.
            con.Close();

            //Return the string.
            return input;

        }

        //The explanation above applies for all methods, where data is being read from the database.


        /// <summary>
        /// Gets the speed of a given cykelrytter.
        /// </summary>
        /// <param name="id">The id of the cykelryte.</param>
        /// <returns></returns>
        public static int cykelRytterSpeed(int id)
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(2);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Gets the endurance of a given cykelrytter.
        /// </summary>
        /// <param name="id">The id if the cykelrytter.</param>
        /// <returns></returns>
        public static int cykelRytterEdnurance(int id)
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(3);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Gets the power of a cykelrytter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int cykelRytterPower(int id)
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(4);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Gets the buying price of a cykelrytter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int cykelRytterPrice(int id)
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(5);
            }
            con.Close();
            return input;
        }


        /// <summary>
        /// Gets the team of a cykelrytter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string cykelRytterTeam(int id)
        {
            //First finds the team value of the cykelrytter.
            con.Open();
            string input = "";
            int index = 0;
            string stm = $"SELECT * FROM cykelrytere WHERE id = '{id}'";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                index = reader.GetInt32(6);
            }

            //Then finds out what team have that value as their id.
            if (index != 0)
            {
                stm = $"SELECT * FROM teams WHERE id = '{index}'";
                com = new SQLiteCommand(stm, con);
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    input = reader.GetString(1);
                }

            }
            else
            {
                input = "No team";
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Sets the team of a given cykelrytter, and updates the databse.
        /// </summary>
        /// <param name="id">The ide of the ckyelrytter.</param>
        /// <param name="input">What team they're being assigned to.</param>
        public static void SetTeams(int id, int input)
        {
            //Opens connection.
            con.Open();

            //The command sent to the database.
            //The cammand asks the database to update the team variable, so it's equal the input, for the cykeryter, with the given id.
            //In the cykelrytere tabel.
            string request = $"UPDATE cykelrytere SET team = {input} WHERE id = {id}";

            //Sends the command to the databse.
            com = new SQLiteCommand(request, con);
            
            //Executes the command.
            com.ExecuteNonQuery();

            //Closes the connection.
            con.Close();
        }

        //The explanation above applies for all methods that updates information in the databse.

        /// <summary>
        /// Saves the player's money to the database..
        /// </summary>
        /// <param name="input">The amount of money the player have.</param>
        public static void SetMoney(int input)
        {
            con.Open();
            string request = $"UPDATE player SET money = {input} WHERE id = 1";
            com = new SQLiteCommand(request, con);
            com.ExecuteNonQuery();
            con.Close();
        }

        /// <summary>
        /// Saves the player's team name to the databse.
        /// </summary>
        /// <param name="input">The player's team name</param>
        public static void SetTeamName(string input)
        {
            con.Open();
            string request = $"UPDATE teams SET name = '{input}' WHERE id = 1";
            com = new SQLiteCommand(request, con);
            com.ExecuteNonQuery();
            con.Close();
        }

        /// <summary>
        /// Gets how much money the player have.
        /// </summary>
        /// <returns></returns>
        public static int GetPlayerMoney()
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM player WHERE id = 1";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(2);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Gets the player's team name.
        /// </summary>
        /// <returns>A string.</returns>
        public static string GetPlayerTeamName()
        {
            //Opens the connections.
            con.Open();

            //Defines the strignt hat gets retruned.
            string input = "";

            //The command that is sent to the databse.
            //The command asks the databse for information from the teams table, from the row where the id is 1.
            string stm = $"SELECT * FROM teams WHERE id = 1";

            //Sends the command to the data base.
            com = new SQLiteCommand(stm, con);

            //Starts the SQLite reader.
            reader = com.ExecuteReader();

            //Whiel the reader is running.
            while (reader.Read())
            {
                //Set the input string to what variable is in the second colon, in the row the command asked infomration about.
                input = reader.GetString(1);
            }

            //Closes the connection.
            con.Close();

            //Retusn the string with the information.
            return input;
        }

        /// <summary>
        /// Gets a name from the table with possible names for the player team.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetATeamName(int id)
        {
            con.Open();
            string input = "";
            string stm = $"SELECT * FROM names WHERE id = {id}";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetString(1);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Saves the counter for how many cyclists the player owns to the database.
        /// </summary>
        /// <param name="input"></param>
        public static void SetPlayerOwnedCount(int input)
        {
            con.Open();
            string request = $"UPDATE player SET count = '{input}' WHERE id = 1";
            com = new SQLiteCommand(request, con);
            com.ExecuteNonQuery();
            con.Close();
        }

        /// <summary>
        /// Gets the number of cyclist owned by the player.
        /// </summary>
        /// <returns></returns>
        public static int GetPlayerOwnedCount()
        {
            con.Open();
            int input = 0;
            string stm = $"SELECT * FROM player WHERE id = 1";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetInt32(3);
            }
            con.Close();
            return input;
        }

        /// <summary>
        /// Resets the databse, only used for debugging.
        /// </summary>
        public static void Reset()
        {
            for (int i = 1; i < 151; i++)
            {   
                SetTeams(i, 0);
            }

            Player.TeamName = " ";
            Player.Money = 5000;
            Player.OwnedCount = 0;
        }

        /// <summary>
        /// Gets the name of a team.
        /// </summary>
        /// <param name="id">The id of the team you want the name of.</param>
        /// <returns></returns>
        public static string GetTameName(int id)
        {
            con.Open();
            string input = "";
            string stm = $"SELECT * FROM teams WHERE id = {id}";
            com = new SQLiteCommand(stm, con);
            reader = com.ExecuteReader();

            while (reader.Read())
            {
                input = reader.GetString(1);
            }
            con.Close();
            return input;
        }
    }
}
